﻿using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Formatting;
using System.Windows;

namespace GitlabIssuePicker.Glyph
{
    internal class GitlabGlyphFactory : IGlyphFactory
    {
        public UIElement GenerateGlyph(IWpfTextViewLine line, IGlyphTag tag) => new GitlabGlyph();
    }
}
