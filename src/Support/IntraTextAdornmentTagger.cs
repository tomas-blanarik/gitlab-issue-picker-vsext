﻿using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Tagging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;

namespace GitlabIssuePicker.Support
{
    internal abstract class IntraTextAdornmentTagger<TData, TAdornment> : ITagger<IntraTextAdornmentTag>
        where TAdornment : UIElement
    {
        protected readonly IWpfTextView View;
        private readonly List<SnapshotSpan> _invalidatedSpans = new List<SnapshotSpan>();
        private Dictionary<SnapshotSpan, TAdornment> _adornmentCache = new Dictionary<SnapshotSpan, TAdornment>();

        protected ITextSnapshot Snapshot { get; private set; }

        protected IntraTextAdornmentTagger(IWpfTextView view)
        {
            View = view;
            Snapshot = view.TextBuffer.CurrentSnapshot;

            View.LayoutChanged += HandleLayoutChanged;
            View.TextBuffer.Changed += HandleBufferChanged;
        }

        protected abstract TAdornment CreateAdornment(TData data, SnapshotSpan span);
        protected abstract bool UpdateAdornment(TAdornment adornment, TData data);
        protected abstract IEnumerable<Tuple<SnapshotSpan, PositionAffinity?, TData>> GetAdornmentData(NormalizedSnapshotSpanCollection spans);

        private void HandleBufferChanged(object sender, TextContentChangedEventArgs args)
        {
            var editedSpans = args.Changes.Select(change => new SnapshotSpan(args.After, change.NewSpan)).ToList();
            InvalidateSpans(editedSpans);
        }

        protected void InvalidateSpans(IList<SnapshotSpan> spans)
        {
            lock (_invalidatedSpans)
            {
                bool wasEmpty = _invalidatedSpans.Count == 0;
                _invalidatedSpans.AddRange(spans);

                if (wasEmpty && _invalidatedSpans.Count > 0)
                {
#pragma warning disable VSTHRD001
                    View.VisualElement.Dispatcher.BeginInvoke(new Action(AsyncUpdate));
#pragma warning restore VSTHRD001
                }
            }
        }

        private void AsyncUpdate()
        {
            if (Snapshot != View.TextBuffer.CurrentSnapshot)
            {
                var translatedAdornmentCache = new Dictionary<SnapshotSpan, TAdornment>();
                Snapshot = View.TextBuffer.CurrentSnapshot;

                foreach (var keyValuePair in _adornmentCache)
                {
                    try
                    {
                        translatedAdornmentCache.Add(
                            keyValuePair.Key.TranslateTo(Snapshot, SpanTrackingMode.EdgeExclusive),
                            keyValuePair.Value);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e);
                    }
                }

                _adornmentCache = translatedAdornmentCache;
            }

            List<SnapshotSpan> translatedSpans;
            lock (_invalidatedSpans)
            {
                translatedSpans = _invalidatedSpans.Select(s => s.TranslateTo(Snapshot, SpanTrackingMode.EdgeInclusive)).ToList();
                _invalidatedSpans.Clear();
            }

            if (translatedSpans.Count == 0)
            {
                return;
            }

            var start = translatedSpans.Select(span => span.Start).Min();
            var end = translatedSpans.Select(span => span.End).Max();

            RaiseTagsChanged(new SnapshotSpan(start, end));
        }

        protected void RaiseTagsChanged(SnapshotSpan span)
        {
            TagsChanged?.Invoke(this, new SnapshotSpanEventArgs(span));
        }

        private void HandleLayoutChanged(object sender, TextViewLayoutChangedEventArgs e)
        {
            SnapshotSpan visibleSpan = View.TextViewLines.FormattedSpan;

            List<SnapshotSpan> toRemove = new List<SnapshotSpan>(
                from keyValuePair
                in _adornmentCache
                where !keyValuePair.Key.TranslateTo(visibleSpan.Snapshot, SpanTrackingMode.EdgeExclusive).IntersectsWith(visibleSpan)
                select keyValuePair.Key);

            foreach (var span in toRemove)
            {
                _adornmentCache.Remove(span);
            }
        }


        public virtual IEnumerable<ITagSpan<IntraTextAdornmentTag>> GetTags(NormalizedSnapshotSpanCollection spans)
        {
            if (spans == null || spans.Count == 0)
            {
                yield break;
            }

            ITextSnapshot requestedSnapshot = spans[0].Snapshot;
            var translatedSpans = new NormalizedSnapshotSpanCollection(spans.Select(span => span.TranslateTo(Snapshot, SpanTrackingMode.EdgeExclusive)));

            foreach (var tagSpan in GetAdornmentTagsOnSnapshot(translatedSpans))
            {
                SnapshotSpan span = tagSpan.Span.TranslateTo(requestedSnapshot, SpanTrackingMode.EdgeExclusive);
                IntraTextAdornmentTag tag = new IntraTextAdornmentTag(tagSpan.Tag.Adornment, tagSpan.Tag.RemovalCallback, tagSpan.Tag.Affinity);

                yield return new TagSpan<IntraTextAdornmentTag>(span, tag);
            }
        }

        private IEnumerable<TagSpan<IntraTextAdornmentTag>> GetAdornmentTagsOnSnapshot(NormalizedSnapshotSpanCollection spans)
        {
            if (spans.Count == 0)
            {
                yield break;
            }

            ITextSnapshot snapshot = spans[0].Snapshot;
            System.Diagnostics.Debug.Assert(snapshot == Snapshot);
            HashSet<SnapshotSpan> toRemove = new HashSet<SnapshotSpan>();

            foreach (var ar in _adornmentCache)
            {
                if (spans.IntersectsWith(new NormalizedSnapshotSpanCollection(ar.Key)))
                {
                    toRemove.Add(ar.Key);
                }
            }

            foreach (var spanDataPair in GetAdornmentData(spans).Distinct(new Comparer()))
            {
                TAdornment adornment;
                SnapshotSpan snapshotSpan = spanDataPair.Item1;
                PositionAffinity? affinity = spanDataPair.Item2;
                TData adornmentData = spanDataPair.Item3;
                if (_adornmentCache.TryGetValue(snapshotSpan, out adornment))
                {
                    if (UpdateAdornment(adornment, adornmentData))
                    {
                        toRemove.Remove(snapshotSpan);
                    }
                }
                else
                {
                    adornment = CreateAdornment(adornmentData, snapshotSpan);
                    if (adornment == null)
                    {
                        continue;
                    }

                    adornment.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                    _adornmentCache.Add(snapshotSpan, adornment);
                }

                yield return new TagSpan<IntraTextAdornmentTag>(
                    snapshotSpan,
                    new IntraTextAdornmentTag(adornment, null, affinity));
            }

            foreach (var snapshotSpan in toRemove)
            {
                _adornmentCache.Remove(snapshotSpan);
            }
        }

        public event EventHandler<SnapshotSpanEventArgs> TagsChanged;

        private class Comparer : IEqualityComparer<Tuple<SnapshotSpan, PositionAffinity?, TData>>
        {
            public bool Equals(Tuple<SnapshotSpan, PositionAffinity?, TData> x, Tuple<SnapshotSpan, PositionAffinity?, TData> y)
            {
                if (x == null && y == null)
                {
                    return true;
                }

                if (x == null || y == null)
                {
                    return false;
                }

                return x.Item1.Equals(y.Item1);
            }

            public int GetHashCode(Tuple<SnapshotSpan, PositionAffinity?, TData> obj)
            {
                return obj.Item1.GetHashCode();
            }
        }

    }
}
