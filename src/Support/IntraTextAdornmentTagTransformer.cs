﻿using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Tagging;
using System;
using System.Collections.Generic;
using System.Windows;

namespace GitlabIssuePicker.Support
{
    internal abstract class IntraTextAdornmentTagTransformer<TDataTag, TAdornment> : IntraTextAdornmentTagger<TDataTag, TAdornment>, IDisposable
        where TDataTag : ITag
        where TAdornment : UIElement
    {
        protected readonly ITagAggregator<TDataTag> dataTagger;
        protected readonly PositionAffinity? adornmentAffinity;

        protected IntraTextAdornmentTagTransformer(
            IWpfTextView view,
            ITagAggregator<TDataTag> dataTagger,
            PositionAffinity adornmentAffinity = PositionAffinity.Successor)
            : base(view)
        {
            this.adornmentAffinity = adornmentAffinity;
            this.dataTagger = dataTagger;

            this.dataTagger.TagsChanged += HandleDataTagsChanged;
        }

        protected override IEnumerable<Tuple<SnapshotSpan, PositionAffinity?, TDataTag>> GetAdornmentData(NormalizedSnapshotSpanCollection spans)
        {
            if (spans.Count == 0)
            {
                yield break;
            }

            ITextSnapshot snapshot = spans[0].Snapshot;
            foreach (IMappingTagSpan<TDataTag> dataTagSpan in dataTagger.GetTags(spans))
            {
                NormalizedSnapshotSpanCollection dataTagSpans = dataTagSpan.Span.GetSpans(snapshot);
                if (dataTagSpans.Count != 1)
                {
                    continue;
                }

                SnapshotSpan span = dataTagSpans[0];
                PositionAffinity? affinity = span.Length > 0 ? null : adornmentAffinity;

                yield return Tuple.Create(span, affinity, dataTagSpan.Tag);
            }
        }

        private void HandleDataTagsChanged(object sender, TagsChangedEventArgs args)
        {
            var changedSpans = args.Span.GetSpans(View.TextBuffer.CurrentSnapshot);
            InvalidateSpans(changedSpans);
        }

        public virtual void Dispose()
        {
            dataTagger.Dispose();
        }
    }
}
