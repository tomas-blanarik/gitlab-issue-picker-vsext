﻿using GitlabIssuePicker.Options;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Tagging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace GitlabIssuePicker.Support
{
    internal abstract class RegexTagger<T> : ITagger<T> where T : ITag
    {
        private IEnumerable<Regex> _matchExpressions;
        private IEnumerable<string> _savedRegexes;

        public RegexTagger(ITextBuffer buffer)
        {
            UpdateRegexes();
            buffer.Changed += (sender, args) => HandleBufferChanged(args);
        }

        public virtual IEnumerable<ITagSpan<T>> GetTags(NormalizedSnapshotSpanCollection spans)
        {
            // update regexes every time they're changed
            UpdateRegexes();
            foreach (var line in GetIntersectingLines(spans))
            {
                var text = line.GetText();
                foreach (var regex in _matchExpressions)
                {
                    foreach (var match in regex.Matches(text).Cast<Match>())
                    {
                        T tag = TryCreateTagForMatch(match);
                        if (tag != null)
                        {
                            SnapshotSpan span = new SnapshotSpan(line.Start + match.Index, line.Start + text.Length);
                            yield return new TagSpan<T>(span, tag);
                        }
                    }
                }
            }
        }
        public event EventHandler<SnapshotSpanEventArgs> TagsChanged;

        void UpdateRegexes()
        {
            var options = GeneralOptions.Instance;
            var regexes = options.SearchRegexes;

            if (_matchExpressions == null || AreRegexesChanged(regexes))
            {
                _savedRegexes = regexes;
                _matchExpressions = regexes.Select(regex =>
                    new Regex(regex, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase));
            }
        }

        bool AreRegexesChanged(string[] currentRegexes)
        {
            if (_savedRegexes.Count() != currentRegexes.Length)
            {
                return true;
            }

            foreach (var currentRegex in currentRegexes)
            {
                if (!_savedRegexes.Contains(currentRegex))
                {
                    return true;
                }
            }

            return false;
        }

        IEnumerable<ITextSnapshotLine> GetIntersectingLines(NormalizedSnapshotSpanCollection spans)
        {
            if (spans.Count == 0)
            {
                yield break;
            }

            int lastVisitedLineNumber = -1;
            ITextSnapshot snapshot = spans[0].Snapshot;
            foreach (var span in spans)
            {
                int firstLine = snapshot.GetLineNumberFromPosition(span.Start);
                int lastLine = snapshot.GetLineNumberFromPosition(span.End);

                for (int i = Math.Max(lastVisitedLineNumber, firstLine); i <= lastLine; i++)
                {
                    yield return snapshot.GetLineFromLineNumber(i);
                }

                lastVisitedLineNumber = lastLine;
            }
        }

        protected abstract T TryCreateTagForMatch(Match match);

        protected virtual void HandleBufferChanged(TextContentChangedEventArgs args)
        {
            if (args.Changes.Count == 0)
            {
                return;
            }

            var temp = TagsChanged;
            if (temp == null)
            {
                return;
            }

            ITextSnapshot snapshot = args.After;

            int start = args.Changes[0].NewPosition;
            int end = args.Changes[args.Changes.Count - 1].NewEnd;

            SnapshotSpan totalAffectedSpan = new SnapshotSpan(
                snapshot.GetLineFromPosition(start).Start,
                snapshot.GetLineFromPosition(end).End);

            temp(this, new SnapshotSpanEventArgs(totalAffectedSpan));
        }
    }
}
