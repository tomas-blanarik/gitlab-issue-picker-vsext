﻿using GitlabIssuePicker.Client;
using GitlabIssuePicker.Options;
using Microsoft.VisualStudio.Shell;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace GitlabIssuePicker.Adornments
{
    internal sealed class IssueInfoAdornment : Grid
    {
        private bool isLoaded = false;
        private string _keyLoaded;
        private TextBlock _block;
        private TextBlock _addressTextBlock;
        private string _webUrl;

        internal IssueInfoAdornment(IssueInfoTag tag)
        {
            _keyLoaded = tag.Key;
            LoadComponents();
            SetOrUpdate(tag);
        }

        private void LoadComponents()
        {
            ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.0, GridUnitType.Star) });
            ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.0, GridUnitType.Star) });
            RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.0, GridUnitType.Star) });
            RowDefinitions.Add(new RowDefinition { Height = new GridLength(1.0, GridUnitType.Star) });

            var imageSource =
                new BitmapImage(new System.Uri("pack://application:,,,/GitlabIssuePicker;component/gitlab-icon-rgb.png"));
            var image = new Image
            {
                Margin = new Thickness(0, 0, 2, 0),
                Source = imageSource,
                Width = 24,
                Height = 24
            };

            image.SetValue(RowProperty, 0);
            image.SetValue(ColumnProperty, 0);

            var brush = new SolidColorBrush(Colors.DarkOrange);
            brush.Freeze();

            var borderBrush = new SolidColorBrush(Colors.Silver);
            borderBrush.Freeze();

            var panel = new StackPanel();
            panel.Children.Add(new TextBlock
            {
                FontWeight = FontWeights.Bold,
                FontSize = 14,
                Margin = new Thickness(0, 0, 0, 5),
                Text = "Open issue in browser"
            });

            panel.Children.Add(new TextBlock
            {
                Text = "When clicked on an issue, you'll be redirected to the gitlab issue details page."
            });

            panel.Children.Add(new Border
            {
                BorderBrush = borderBrush,
                BorderThickness = new Thickness(0, 1, 0, 0),
                Margin = new Thickness(0, 0, 5, 0)
            });

            var wrapPanel = new WrapPanel();
            wrapPanel.Children.Add(new Image
            {
                Margin = new Thickness(0, 0, 5, 0),
                Source = imageSource,
                Width = 24,
                Height = 24
            });

            _addressTextBlock = new TextBlock
            {
                FontStyle = FontStyles.Italic,
                Margin = new Thickness(0, 4, 0, 0)
            };

            wrapPanel.Children.Add(_addressTextBlock);
            panel.Children.Add(wrapPanel);

            _block = new TextBlock
            {
                Margin = new Thickness(0, 4, 0, 0),
                FontSize = 11,
                FontWeight = FontWeights.Bold,
                Foreground = brush,
                Cursor = Cursors.Hand,
                ToolTip = panel
            };

            _block.SetValue(RowProperty, 0);
            _block.SetValue(ColumnProperty, 1);

            Children.Add(image);
            Children.Add(_block);

            MouseLeftButtonUp += IssueInfoAdornment_MouseLeftButtonUp;
            MouseEnter += IssueInfoAdornment_MouseEnter;
            MouseLeave += IssueInfoAdornment_MouseLeave;
        }

        private void IssueInfoAdornment_MouseEnter(object sender, MouseEventArgs e)
        {
            var brush = new SolidColorBrush(Colors.OrangeRed);
            brush.Freeze();
            _block.Foreground = brush;
        }

        private void IssueInfoAdornment_MouseLeave(object sender, MouseEventArgs e)
        {
            var brush = new SolidColorBrush(Colors.DarkOrange);
            brush.Freeze();
            _block.Foreground = brush;
        }

        private void IssueInfoAdornment_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!string.IsNullOrEmpty(_webUrl))
            {
                Process.Start($"{_webUrl}");
            }
        }

        internal void SetOrUpdate(IssueInfoTag tag)
        {
            if ((_keyLoaded != tag.Key && isLoaded) || !isLoaded)
            {
                if (isLoaded)
                {
                    isLoaded = false;
                }

                var options = GeneralOptions.Instance;
                var url = options.GitlabHost;
                var pwd = options.PersonalToken;

                if (!isLoaded)
                {
                    ThreadHelper.JoinableTaskFactory.RunAsync(async () =>
                    {
                        var gitlabClient = new GitlabClient(url, pwd);
                        if (int.TryParse(tag.Key.Replace("#", ""), out int iid))
                        {
                            var issue = await gitlabClient.GetIssueAsync(iid);
                            if (issue != null)
                            {
                                isLoaded = true;
                                _webUrl = issue.WebUrl;
                                _block.Text = issue.ToString();
                                _addressTextBlock.Text = _webUrl;

                                if (_keyLoaded != tag.Key)
                                {
                                    _keyLoaded = tag.Key;
                                }
                            }
                        }
                    });
                }
            }
        }
    }
}
