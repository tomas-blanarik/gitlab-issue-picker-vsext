﻿using GitlabIssuePicker.Support;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Tagging;
using System;
using System.Collections.Generic;

namespace GitlabIssuePicker.Adornments
{
    internal sealed class IssueInfoAdornmentTagger

#if HIDING_TEXT
        : IntraTextAdornmentTagTransformer<IssueInfoTag, IssueInfoAdornment>
#else
        : IntraTextAdornmentTagger<IssueInfoTag, IssueInfoAdornment>
#endif

    {
        internal static ITagger<IntraTextAdornmentTag> GetTagger(IWpfTextView view, Lazy<ITagAggregator<IssueInfoTag>> tagger)
        {
            return view.Properties.GetOrCreateSingletonProperty(() => new IssueInfoAdornmentTagger(view, tagger.Value));
        }

#if HIDING_TEXT
        private IssueInfoAdornmentTagger(IWpfTextView view, ITagAggregator<IssueInfoTag> tagger)
            : base(view, tagger)
        { }

        public override void Dispose()
        {
            base.view.Properties.RemoveProperty(typeof(IssueInfoAdornmentTagger));
        }
#else
        private readonly ITagAggregator<IssueInfoTag> _tagger;

        private IssueInfoAdornmentTagger(IWpfTextView view, ITagAggregator<IssueInfoTag> tagger)
            : base(view)
        {
            _tagger = tagger;
        }

        public void Dispose()
        {
            _tagger.Dispose();
            View.Properties.RemoveProperty(typeof(IssueInfoAdornmentTagger));
        }

        protected override IEnumerable<Tuple<SnapshotSpan, PositionAffinity?, IssueInfoTag>> GetAdornmentData(NormalizedSnapshotSpanCollection spans)
        {
            if (spans.Count == 0)
            {
                yield break;
            }

            ITextSnapshot snapshot = spans[0].Snapshot;
            var tags = _tagger.GetTags(spans);

            foreach (IMappingTagSpan<IssueInfoTag> dataTagSpan in tags)
            {
                NormalizedSnapshotSpanCollection tagSpan = dataTagSpan.Span.GetSpans(snapshot);
                if (tagSpan.Count != 1)
                {
                    continue;
                }

                SnapshotSpan adornmentSpan = new SnapshotSpan(tagSpan[0].End, 0);
                yield return Tuple.Create(adornmentSpan, (PositionAffinity?)PositionAffinity.Successor, dataTagSpan.Tag);
            }
        }
#endif

        protected override IssueInfoAdornment CreateAdornment(IssueInfoTag dataTag, SnapshotSpan span)
        {
            return new IssueInfoAdornment(dataTag);
        }

        protected override bool UpdateAdornment(IssueInfoAdornment adornment, IssueInfoTag dataTag)
        {
            adornment.SetOrUpdate(dataTag);
            return true;
        }
    }
}
